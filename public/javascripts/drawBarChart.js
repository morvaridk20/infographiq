//simplebarchart
//
//
//
//

function simplebarchart(data2, selected_dimensions, columns_string)
{

	if (data2.length < 1)
		return;
	var data3 = [];
	for (i = 0; i < data2[0].length; i++) {
		data3[i] = {
			first : data2[0][i],
			second : data2[1][i]
		};
	}
	d3.select("svg").remove();
	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();
	var margin = {
		top : 40,
		right : 20,
		bottom : 30,
		left : 40
	},
	width = 900 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

	var formatPercent = d3.format(".0%");

	var x = d3.scale.ordinal()
		.rangeRoundBands([0, width], .1);

	var y = d3.scale.linear()
		.range([height, 0]);

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left")

		var tip = d3.tip()
		.attr('class', 'd3-tip')
		.offset([-10, 0])
		.html(function (d) {
			return "<strong>" + columns_string[0] + ":</strong> <span style='color:red'>" + d.first + "</span>";
		})

		var svg = d3.select("#chart").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	svg.call(tip);

	x.domain(data3.map(function (d) {
			return d.first;
		}));
	y.domain([0, d3.max(data3, function (d) {
				return d.second;
			})]);

	svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + height + ")")
	//.style("text-anchor", "end")
	//.text(columns_string[0])
	.attr("visibility", "hidden")
	.call(xAxis);

	svg.append("g")
	.attr("class", "y axis")
	.call(yAxis)
	.append("text")
	.attr("transform", "rotate(-90)")
	.attr("y", 6)
	.attr("dy", ".71em")
	.style("text-anchor", "end")
	.text(columns_string[1])

	svg.selectAll(".bar")
	.data(data3)
	.enter().append("rect")
	.attr("class", "bar")
	.attr("x", function (d) {
		return x(d.first);
	})
	.attr("width", x.rangeBand())
	.attr("y", function (d) {
		return y(d.second);
	})
	.attr("height", function (d) {
		return height - y(d.second);
	})
	.on('mouseover', tip.show)
	.on('mouseout', tip.hide)

}
