// piechart2

function piechart2(data2, selected_dimensions, columns_string) {
	var data3 = [];
	for (i = 0; i < 90; i++) {
		data3[i] = {
			first : data2[0][i],
			second : data2[1][i]
		};
	}
	d3.select("svg").remove();
	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();
	var width = 900,
	height = 500,
	radius = Math.min(width, height) / 2;

	//var color = d3.scale.category20b();

	var color = d3.scale.ordinal()
		.range(["#D24D57", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00", "#F22613", "#C0392B", "#E26A6A", "#913D88", "#E4F1FE", "#DB0A5B", "#52B3D9", "D91E18", "#F89406", "#2C3E50", "#9A12B3", "#22A7F0", "#C5EFF7", "#96281B", "#F9690E", "#DCC6E0", "#BF55EC", "#663399", "#EF4836", "#3498DB", "#1BBC9B", "#3FC380", "#6BB9F0", "#BE90D4", "#90C695", "#03C9A9", "#674172", "#AEA8D3", "#D35400", "#8E44AD", "#87D37C"]);

	var arc = d3.svg.arc()
		.outerRadius(radius - 10)
		.innerRadius(0);

	var arcOver = d3.svg.arc()
		.outerRadius(radius + 5);

	var pie = d3.layout.pie()
		.sort(null)
		.value(function (d) {
			return d.second;
		});

	//   var tip = d3.tip()
	//  .attr('class', 'd3-tip')
	//  .offset([-10, 0])
	//  .html(function(d) {
	//    return "<strong>"+columns_string[0]+":</strong> <span style='color:red'>" + d.data.first + "</span>" +" "+ "<strong>"+columns_string  [1]+":</strong> <span style='color:red'>" + d.data.second + "</span>" ;
	//  })

	var svg = d3.select("#chart").append("svg")
		.attr("width", width)
		.attr("height", height)
		.append("g")
		.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
/*
	svg.append("rect").transition().duration(500)
	.attr("width", 210)
	.attr("height", 50)
	.attr("x", 240)
	.attr("y", -150)
	.style("fill", "none")
	.style("stroke-width", 1)
	.attr("stroke", "black");
*/
	//  svg.call(tip);


	data3.forEach(function (d) {
		d.second = +d.second;
	});

	var div = d3.select("body").append("div")
		.attr("class", "tooltip")
		.style("opacity", 0);

	var g = svg.selectAll(".arc")
		.data(pie(data3))
		.enter().append("g")
		.attr("class", "arc")

		g.append("path")
		.attr("d", arc)
		.style("fill", function (d) {
			return color(d.data.first);
		})

		.on("mouseover", function (d) {
			d3.select(this)
			.attr("r", 10)
			//.style("fill", "red")
			.style("opacity", 0.8);
			div.transition()
			.duration(200)
			.style("opacity", 1);
			div.html(
				"<strong>" + columns_string[0] + ":</strong> <span style='color:red'>" + "<strong>" + d.data.first + "</strong>" + "</span>" + " " + "<strong>" + columns_string[1] + ":</strong> <span style='color:red'>" + "<strong>" + d.data.second + "</strong> </span>")
			.style("left", (d3.event.pageX-185) + "px")
			.style("top", (d3.event.pageY-20) + "px");
		})
		.on("mouseout", function (d) {
			d3.select(this)
	//		.style("opacity", 1.2)
	//		.style("fill", function (d) {
	//			return (d.data.first);
	//		})
			//.attr("r", 10)
			//.style("fill", "red")
			div.transition()
			.duration(500)
			.style("opacity", 0);
		})

		.on("mouseenter", function (d) {
			d3.select(this)
			.attr("stroke", "black")
			.transition()
			.duration(1000)
			.attr("d", arcOver)
			.attr("stroke-width", 1);
		})
		.on("mouseleave", function (d) {
			d3.select(this).transition()
			.attr("d", arc)
			.attr("stroke", "none");
		}); ;
	/*	.on("click", function (d) {
	d3.select(this)
	.attr("stroke","white")
	.transition()
	.duration(1000)
	.attr("d", arcOver)
	.attr("stroke-width",6);
	}); */

	/*
	function(d) {
	d3.select(this)
	.style ("fill", "red");

	.on("click", click);

	function click(d) {
	path.transition()
	.duration(750)*/
	//     });

}
