
//simplelinechart
//
//
//
//
function linechart2(data2, selected_dimensions, columns_string) {
	if (data2.length < 1)
		return;
	var data3 = [];
	for (i = 0; i < data2[0].length; i++) {
		data3[i] = {
			first : data2[0][i],
			second : data2[1][i]
		};
	}

	d3.select("svg").remove();
	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();
	console.log(data3);
	/*
	data3.sort( function (a,b){
	if (a.first > b.first) {
	return 1;
	}
	if (a.first < b.first) {
	return -1;
	}
	// a must be equal to b
	return 0;

	});

	 */

	var margin = {
		top : 20,
		right : 20,
		bottom : 30,
		left : 100
	},
	width = 900 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

	//        var x = d3.scale.ordinal().rangeRoundBands([width, 0]);

	var x = d3.scale.linear()
		.range([0, width]);

	var y = d3.scale.linear()
		.range([height, 0]);

	var xAxis = d3.svg.axis().scale(x).orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");

	var line = d3.svg.line()
		.x(function (d) {
			return x(d.first);
		})
		.y(function (d) {
			return y(d.second);
		});

	var svg = d3.select("#chart").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	data3.forEach(function (d) {
		d.first = d.first;
		d.second = +d.second;
	});

	//        console.log(data3);

	//        x.domain(data3.map(function(d) { return d.first; }));
	//        x.domain(d3.extent(data3, function(d) { return d.first; }));
	//	x.domain([0, d3.max(data3, function(d) { return d.first; })]);
	x.domain(d3.extent(data3, function (d) {
			return d.first;
		}));
	y.domain(d3.extent(data3, function (d) {
			return d.second;
		}));

	svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + height + ")")
	.call(xAxis)
	.append("text")
	.attr("class", "label")
	.attr("x", width)
	.attr("y", -6)
	.style("text-anchor", "end")
	.text(columns_string[0]);

	svg.append("g")
	.attr("class", "y axis")
	.call(yAxis)
	.append("text")
	.attr("transform", "rotate(-90)")
	.attr("y", 6)
	.attr("dy", ".71em")
	.style("text-anchor", "end")
	.text(columns_string[1]);

	svg.append("path")
	.datum(data3)
	.attr("class", "line")
	.attr("d", line);

}
