function drawpc(data2, selected_dimensions, columns_string) {

	d3.select("svg").remove();

	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();

	if (data2.length < 1)
		return;
	
	var data3 = new Array();

	for (i = 0; i < 20; i++) {
		
		var obj1 = {};
				
		obj1[columns_string[0]] = data2[0][i];
		obj1[columns_string[1]] = data2[1][i];
				
		data3[i] = obj1;		
	}

	var pdiv = document.createElement("div");
	pdiv.type = "range";
	pdiv.id = "pdiv";
	pdiv.className = "parcoords";
	pdiv.style.width = "1000px";
	pdiv.style.height = "400px";
	var div = document.getElementById("chart");
	div.appendChild(pdiv);

	var data4 = [];
	for (i = 0; i < data2[0].length; i++) {
		data4.push([data2[0][i], data2[1][i]])
	}
	var pc = d3.parcoords()("#pdiv")
		.data(data3)
		.render()
		.ticks(3)
		.createAxes()
		.reorderable()
		.brushMode("1D-axes");
	
	pdiv.style.width = "40px";

}

function drawpc3(data2, selected_dimensions, columns_string) {
	d3.select("svg").remove();
	
	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();

	if (data2.length < 1)
		return;
	var data3 = [];

	for (i = 0; i < 20; i++) {
		var obj = {};
		
		obj[columns_string[0]] = data2[0][i];
		obj[columns_string[1]] = data2[1][i];
		obj[columns_string[2]] = data2[2][i];
				
		data3[i] = obj;
	}

	var pdiv = document.createElement("div");
	pdiv.type = "range";
	pdiv.id = "pdiv";
	pdiv.className = "parcoords";
	pdiv.style.width = "900px";
	pdiv.style.height = "400px";
	var div = document.getElementById("chart");
	div.appendChild(pdiv);

	var data4 = [];
	for (i = 0; i < data2[0].length; i++) {
		data4.push([data2[0][i], data2[1][i]])
	}

		
	var pc = d3.parcoords()("#pdiv")
		.data(data3)
		.render()
		.ticks(3)
		.createAxes()
		.reorderable()
		.brushMode("1D-axes");

	pdiv.style.width = "40px";
}

function drawpc4(data2, selected_dimensions, columns_string) {
	d3.select("svg").remove();

	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();

	if (data2.length < 1)
		return;
	var data3 = [];

	for (i = 0; i < 20; i++) {
		
		var obj = {};
		
		obj[columns_string[0]] = data2[0][i];
		obj[columns_string[1]] = data2[1][i];
		obj[columns_string[2]] = data2[2][i];
		obj[columns_string[3]] = data2[3][i];
				
		data3[i] = obj;
	}

	var pdiv = document.createElement("div");
	pdiv.type = "range";
	pdiv.id = "pdiv";
	pdiv.className = "parcoords";
	pdiv.style.width = "1000px";
	pdiv.style.height = "400px";
	var div = document.getElementById("chart");
	div.appendChild(pdiv);

	console.log(data3);

	var data4 = [];
	for (i = 0; i < data2[0].length; i++) {
		data4.push([data2[0][i], data2[1][i]])
	}

	//console.log(data4);
	var pc = d3.parcoords()("#pdiv")
		.data(data3)
		.render()
		.ticks(3)
		.createAxes()
		.reorderable()
		.brushMode("1D-axes");
	pdiv.style.width = "30px";
}


function drawMDS(data2){
    console.log(data2);
    $("#sliderx").hide();
	
    
    console.log("data2.length"+data2.length);
	var data3 = [];
	for (i = 0; i < data2.length; i++) {
		data3[i] = {
				first : (data2[i][1]),
				second : (data2[i][2]),
				third: (data2[i][0]),
				fourth: (data2[i][3]),
				fifth: (data2[i][4]),
				sixth: (data2[i][5]),
			
		};
	}

	
	console.log(data3);
	d3.select("svg").remove();
	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();
	var margin = {
		top : 20,
		right : 20,
		bottom : 30,
		left : 40
	},
	width = 900 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

	var x = d3.scale.linear()
		.range([0, width - 100]);

	var y = d3.scale.linear()
		.range([height, 0]);

	var color = d3.scale.category10();

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");

	var svg = d3.select("#chart").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	data3.forEach(function (d) {
		d.second = +d.second;
		d.first = +d.first;
	});

	x.domain(d3.extent(data3, function (d) {
			return d.first;
		})).nice();
	y.domain(d3.extent(data3, function (d) {
			return d.second;
		})).nice();

	svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + height + ")")
	.call(xAxis)
	.append("text")
	.attr("class", "label")
	.attr("x", width - 100)
	.attr("y", -6)
	.style("text-anchor", "end")
	.text("X-axis");

	svg.append("g")
	.attr("class", "y axis")
	.call(yAxis)
	.append("text")
	.attr("class", "label")
	.attr("transform", "rotate(-90)")
	.attr("y", 6)
	.attr("dy", ".71em")
	.style("text-anchor", "end")
	.text("Y-axis")

	var div = d3.select("body").append("div")
		.attr("class", "tooltip")
		.style("opacity", 0);

	var tip = d3.tip()
	.attr('class', 'd3-tip')
	.offset([-10, 0])
	.html(function (d) {
		return "<span style='font-size:10px;'>" + "<strong style='font-size:16px;color:red'>" + d.third + "</strong> <br/> GDP : " + d.fourth+
											" billion US$<br/> Corruption Index : " + d.fifth +
											"<br/> Population : " + d.sixth +" million</span>";
	})
	
	svg.call(tip);
	
	svg.selectAll(".dot")
	.data(data3)
	.enter().append("circle")
	.attr("class", "dot")
	.attr("r", 3.5)
	.attr("cx", function (d) {
		return x(d.first);
	})
	.attr("cy", function (d) {
		return y(d.second);
	})
	.style("fill", "steelblue")
	.on('mouseover', tip.show)
	.on('mouseout', tip.hide)


}
