
function visualtest()
{
	$.post("selectAll",
			  function(data,status){
				drawpc(data);
			  });



}


function MDSdraw()
{
	$.post("buildDataMap",
			  function(data,status){
		//		console.log(data);
                drawMDS(data);
			  });

}

function drawingHandler(){
	
	if ($('#bar-chart').css('opacity')!=1){
		if( $("#x-axis option:selected").text()!="None" && $("#y-axis option:selected").text()!="None")
			selectColumns();
	}
	else if ($('#scatter-chart').css('opacity')!=1){
		if( $("#x-axis option:selected").text()!="None" && $("#y-axis option:selected").text()!="None")
			selectColumns();
	}
	else if ($('#parallel-coordinates').css('opacity')!=1){
		if( $("#x-axis option:selected").text()!="None" && $("#y-axis option:selected").text()!="None")
			selectColumns();
	}
	else if ($('#pie-chart').css('opacity')!=1){
		if( $("#color-axis option:selected").text()!="None" && $("#size-axis option:selected").text()!="None")
			selectColumns();
	}
	else if ($('#line-chart').css('opacity')!=1){
		if( $("#x-axis option:selected").text()!="None" && $("#y-axis option:selected").text()!="None")
			selectColumns();
	}
}

function selectColumns()
{
	$( "#error" ).removeClass("ui warning message").removeClass( "ui hidden message" );
	document.getElementById("error").innerHTML="";
	
	var dimensions_name = ["x-axis", "y-axis", "color-axis", "size-axis"]; 
	// This array contains selected dimensions like this x-axis-> ups 
	var selected_dimensions =[];
	
	//This is only the name of selected columns for selection from DB  
	var columns_string=[];
	$.each(dimensions_name, function( index, value ) {
		if($("#" +value+" option:selected").text() != "None") {
			selected_dimensions.push(value);
			columns_string.push($("#" + value+" option:selected").text());
		}
	});
	
	 //This stores the scales of selected columns 
	 var data_scale= [];
	 $.each(columns_string, function( index, value ) {
			//checks if it is derived attribute an set it as ratio
		if((value.indexOf('SUM') > -1)||( value.indexOf('COUNT') > -1)||
					(value.indexOf('AVG') > -1)||(value.indexOf('MIN') > -1)||
					(value.indexOf('MAX') > -1)){
				data_scale.push("Ratio");
			}	
		else if($("#" + value+" option:selected").text()) {
				data_scale.push($("#" + value+" option:selected").text());
			}
		
		});	 

	// Gets the selected columns from DB	 
	if(checkDimensionValues(columns_string) ==1){
		$.post("selectColumns",
			  {
			    columns: columns_string
			  },
			  function(data,status){
				// check if the requested diagram is feasible with these conditions
				findDiagramType(data_scale,data,selected_dimensions,columns_string)
			  });
	}		
}

function saveJsonData (array){
	    var ret = [];
	    $.each(array,function(i,data){
	        ret.push(data);
	    });
	    return ret;
	};

function checkDimensionValues(columns_string){
	 
	 var flag = 1;
	 //x and y should always be selected except for when it is a pie chart
	 if($('#pie-chart').css('opacity') ==0.6){
		if( $("#x-axis option:selected").text()=="None"||$("#y-axis option:selected").text()=="None"){
			$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
			document.getElementById("error").innerHTML="You need to choose at least X-Axis and Y-Axis!";
			flag=0; 
			return flag;
		}
	 }
	
	if (columns_string.length == 0){
		$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
		document.getElementById("error").innerHTML="Please choose the fields that you want to visualize!";
		flag=0;
		return flag;
	}
	
	return flag;
}

function findDiagramType(data_scale,data,selected_dimensions,columns_string){
		
		
	//finds if there are nominal, ratio pr interval scale in the data
	var nominal_found = $.inArray('Nominal', data_scale) > -1;
	var ratio_found = $.inArray('Ratio', data_scale) > -1;
	var interval_found = $.inArray('Interval', data_scale) > -1;
	var ordinal_found = $.inArray('Ordinal', data_scale) > -1;
		
	var nr_nominals =0;
	var nr_ratios =0;
	var nr_intervals =0;
	var nr_ordinals =0;
	// count the number of nominals, ratios, intervals and ordinals
	for(i=0; i<data_scale.length ;i++){
			 
		 if(data_scale[i].indexOf("Nominal") > -1)
			 nr_nominals++;
		 if(data_scale[i].indexOf("Ratio") > -1)
			 nr_ratios++;
		 if(data_scale[i].indexOf("Interval") > -1)
			 nr_intervals++;
		 if(data_scale[i].indexOf("Ordinal")  > -1)
			 nr_ordinals++;
	 }
		
	//// checking possibilities with different scales 
	if ($('#bar-chart').css('opacity')!=1){
		
		// user has selected 2 dimensions 
		if(data_scale.length==2){
			if(nominal_found&& (ratio_found ||interval_found || ordinal_found ))
				simplebarchart(data,selected_dimensions,columns_string);
			else{
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Please Try Again! You need one nominal value and one of either ratio, interval or ordinal.";
			}
			}
		// user has selected 3 dimensions
		else if (data_scale.length==3){
			if((nr_nominals==1)&&((nr_ratios==2)||(nr_ordinals==2)||(nr_intervals==2))){
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Sorry! No available diagram with your selection.";
			}else{
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");			
				document.getElementById("error").innerHTML="Please Try Again! You need one nominal value and two of either ratio, interval or ordinal.";
			}
		}
		// user has selected 4 dimensions
		else if (data_scale.length==4){
			if((nr_nominals==1)&&(ratio_found ||interval_found || ordinal_found )&&((nr_ratios==3)||(nr_ordinals==3)||(nr_intervals==3)))
			{	
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Sorry! No available diagram with your selection.";
			}else{
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Please Try Again! You need one nominal value and three of either ratio, interval or ordinal.";
			}
		}
		else{
			$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
			document.getElementById("error").innerHTML="Sorry! No possible diagram with your selection";	
		}
	}else if ($('#scatter-chart').css('opacity')!=1){
		// user has selected 2 dimensions 
		if(data_scale.length==2){
				scatterPlot2(data,selected_dimensions,columns_string,data_scale);
		}
		// user has selected 3 dimensions 
		else if (data_scale.length==3){
			if((nr_ratios+nr_ordinals>=2)||(nr_ordinals+nr_intervals>=2)||(nr_intervals+nr_ratios>=2))
				scatterPlot3(data,selected_dimensions,columns_string);
			else {
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Please Try Again! Hint: You need at least 2 values of any of the ordinal, interval or ratio scale.";
			}		
		}
		// user has selected 4 dimensions
		else if (data_scale.length==4){
			if((nr_nominals==2)&&((nr_ratios+nr_ordinals==2)||(nr_ordinals+nr_intervals==2)||(nr_intervals+nr_ratios==2)))
				scatterPlot4(data,selected_dimensions,columns_string);
			
			else if(nominal_found&&(ratio_found||interval_found)&&((nr_ratios+nr_ordinals>=2)||(nr_ordinals+nr_intervals>=2)||(nr_intervals+nr_ratios>=2)))
				scatterPlot4(data,selected_dimensions,columns_string);
			else {
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Please Try Again! Hint: You need at least 2 values of any of the ordinal, interval or ratio scale and one nominal value.";
			}
			}
		else{
			$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
			document.getElementById("error").innerHTML="Sorry! No available diagram with your selection.";
		}
//head
        }else if ($('#parallel-coordinates').css('opacity')!=1){
		// user has selected 2 dimensions 
		if(data_scale.length==2){
				drawpc(data,selected_dimensions,columns_string);
		}
		// user has selected 3 dimensions 
		else if (data_scale.length==3){
			
				drawpc3(data,selected_dimensions,columns_string);	
		}
		// user has selected 4 dimensions
		else if (data_scale.length==4){
                drawpc4(data,selected_dimensions,columns_string);
			}
		else{
			$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
			document.getElementById("error").innerHTML="Sorry! No available diagram with your selection.";
		}
//  end		
	}else if ($('#pie-chart').css('opacity')!=1){
			
		// user has selected 2 dimensions 
		if((nr_nominals==1)&&((nr_ratios==1)||(nr_intervals==1)))
			piechart2(data,selected_dimensions,columns_string);
		else{
			$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
			document.getElementById("error").innerHTML="Please Try Again! Hint: You need one nominal value and one of either ordinal, interval or ratio.";	
		}		
	}else if ($('#line-chart').css('opacity')!=1){
		
		// user has selected 2 dimensions 
		if(data_scale.length==2){
			
			if((nr_ordinals==1)&&(interval_found||ratio_found))
				linechart2(data,selected_dimensions,columns_string);
			else{
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Please Try Again! Hint: You need one ordinal value and one of either ordinal or ratio.";	
			}
				
		}
		// user has selected 3 dimensions 
		else if (data_scale.length==3){
			
			if((nr_ordinals==1)&&(nr_intervals==1||nr_ratios==1)&&(nr_nominals==1)){
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Sorry! No available diagram with your selection.";
			}
			else{
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Please Try Again! Hint: You need one ordinal value, one nominal value and one of either ordinal or ratio.";	
			}
		
		}
		// user has selected 4 dimensions 
		else if (data_scale.length==4){
			
			if((nr_ordinals==1)&&(nr_nominals==1)&&(nr_intervals+nr_ratios==2)){
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Sorry! No available diagram with your selection.";
			}
			else{
				$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
				document.getElementById("error").innerHTML="Please Try Again! Hint: You need one ordinal value, one nominal value and two of either ordinal or ratio.";	
			}
			}
		else{
			$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
			document.getElementById("error").innerHTML="Sorry! No available diagram with your selection.";
		}
		
	}
	else{
		$( "#error" ).removeClass( "ui hidden message" ).addClass("ui warning message");
		document.getElementById("error").innerHTML="Sorry! No available diagram with your selection.";

	}
}
