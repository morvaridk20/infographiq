function selectQuery(counter){

	var functionSelection=["Average","Minimum","Maximum","Sum","Count"];
	// this one contains the part of query we will have in select
	var queryParts=[];

	if($("#groupbyfields option:selected").text() != "---") {
		queryParts.push($("#groupbyfields option:selected").text());
	}
	
	for(i=1;i<counter+1;i++){
			if($("#functions"+i+" option:selected").text() != "---") {
				if($("#fields"+i+" option:selected").text() != "---") {
					var functionString;
					var functionText=$("#functions"+i+" option:selected").text();
					
					if (functionText.indexOf('Average') > -1)
						functionString="AVG(";
					else if (functionText.indexOf('Minimum') > -1)
						functionString="MIN(";
					else if (functionText.indexOf('Maximum') > -1)
						functionString="MAX(";
					else if (functionText.indexOf('Sum') > -1)
						functionString="SUM(";
					else if (functionText.indexOf('Count') > -1)
						functionString="COUNT(";
					
					functionString = functionString+$("#fields"+i+" option:selected").text()+")";
					queryParts.push(functionString);
				}
			}
	}
	// print query parts to console 
	console.log(queryParts);
	
	// Run the query on server side 
	var result;
	if (true){
		$.post("buildQuery", {
			querySelectParts : queryParts
		}, function(data, status) {
			// Puts the query into the query box
			result = data;
		});
	}
}

function queryErrorCheck(counter){
	if (($("#fields"+counter+" option:selected").text() == "---")|| ($("#functions"+counter+" option:selected").text() == "---")){
		$( "#errorQuery" ).removeClass( "ui hidden message" ).addClass("ui warning message");
		document.getElementById("errorQuery").innerHTML="Choosing aggregation function, target field and group by is required!";
		return false;
	}
	else {
		$( "#errorQuery" ).removeClass("ui warning message").removeClass( "ui hidden message" );
		document.getElementById("errorQuery").innerHTML="";
		return true;
	}
		
} 