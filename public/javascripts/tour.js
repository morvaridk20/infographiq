	



			var tour = new Tour({
				  name: "tour",
				  steps: [],
				  container: "body",
				  keyboard: true,
				  storage: window.localStorage,
				  debug: false,
				  backdrop: false,
				  backdropPadding: 0,
				  redirect: true,
				  orphan: true,
				  duration: false,
				  delay: false,
				  basePath: "",

				  
				
			
			  steps: [
			  {
			    title: "Welcome",
			    content: "Welcome to InfographiQ, take this tour to familiarize yourself with the website."
			  },
			  {
				placement: "bottom", 
			    element: "div#selectionPage",
			    title: "Choose Scale",
			    content: "Here you see sample data of your selected csv file. Please choose the approirate scale for each column."
			  },
			 {
				placement: "top", 
			    element: "div#calculation-container",
			    title: "Optional Aggregation",
			    content: "This is an optional aggregation choice available if you would like to perform maximum, minimum, sum and count on your selected columns. With the + and - buttons you can add and delete your selected aggregations"
			  },  
			  {
					placement: "top", 
				    element: "button#proceed-btn.ui.primary.button",
				    title: "Proceed to visualization",
				    content: "Click here to proceed to visualization step.",
				    reflex: "true"
				    
					  }
					  
			  
			],
			  backdrop: true,
			  storage: false
			});

			tour.init();

			tour.start();
			
			

	