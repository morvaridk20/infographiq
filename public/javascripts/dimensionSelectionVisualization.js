function mdsDisplay(){
	$("#x-axis").parent('li').hide();
	$("#x-label").hide();
	$("#y-axis").parent('li').hide();	
	$("#y-label").hide();
	$("#color-axis").parent('li').hide();
	$("#color-label").hide();
	$("#size-axis").parent('li').hide();
	$("#size-label").hide();
	
	var x_index = 0;
	var y_index = 0;
	var color_index = 0;
	var size_index = 0;
	
	$('#x-axis').empty().append('<option value="0">None</option></select>');
	$('#y-axis').empty().append('<option value="0">None</option></select>');
	$('#color-axis').empty().append('<option value="0">None</option></select>');
	$('#size-axis').empty().append('<option value="0">None</option></select>');
	
	$.each(selectColumns_Name_Value, function( index, value ) {
		
		
		$('#x-axis').append('<option value="' + ++x_index + '">' + index + '</option>');
		
		$('#y-axis').append('<option value="' + ++y_index + '">' + index + '</option>');	
		
		if(value == "Nominal") {
			$('#color-axis').append('<option value="' + ++color_index + '">' + index + '</option>');	
		}
		
		if(value == "Ratio" || value == "Interval") {
			$('#size-axis').append('<option value="' + ++size_index + '">' + index + '</option>');	
		}
		
	});
}
function parallelDisplay(){
	$("#x-axis").parent('li').show();
	$("#x-label").show();
	$("#x-label").empty();
	$("#x-label").append("First-Axis");
	$("#y-axis").parent('li').show();	
	$("#y-label").show();
	$("#y-label").empty();
	$("#y-label").append("Second-Axis");
	$("#color-axis").parent('li').show();
	$("#color-label").show();
	$("#color-label").empty();
	$("#color-label").append("Third-Axis");
	$("#size-axis").parent('li').show();
	$("#size-label").show();
	$("#size-label").empty();
	$("#size-label").append("Fourth-Axis");
	
	var x_index = 0;
	var y_index = 0;
	var color_index = 0;
	var size_index = 0;
	
	$('#x-axis').empty().append('<option value="0">None</option></select>');
	$('#y-axis').empty().append('<option value="0">None</option></select>');
	$('#color-axis').empty().append('<option value="0">None</option></select>');
	$('#size-axis').empty().append('<option value="0">None</option></select>');
	
	$.each(selectColumns_Name_Value, function( index, value ) {
		
		
		$('#x-axis').append('<option value="' + ++x_index + '">' + index + '</option>');
		
		$('#y-axis').append('<option value="' + ++y_index + '">' + index + '</option>');	
		
		$('#color-axis').append('<option value="' + ++color_index + '">' + index + '</option>');	
	
		
		$('#size-axis').append('<option value="' + ++size_index + '">' + index + '</option>');	
		
	});
}
function parallelDisplay2(){
	
	var result=[];
	$.get("getDerivedFields", function(data){
				   	result = data;
				   	var counter = 0;
					$.each( result, function( index, value ) {
						if (counter == 0){
							$("#x-axis").parent('li').show();
							$("#x-label").show();
							$("#x-label").empty();
							$("#x-label").append("First-Axis");
							$("#y-axis").parent('li').show();
							$("#y-label").show();
							$("#y-label").empty();
							$("#y-label").append("Second-Axis");
							$("#color-axis").parent('li').show();
							$("#color-label").show();
							$("#color-label").empty();
							$("#color-label").append("Third-Axis");
							$("#size-axis").parent('li').show();
							$("#size-label").show();
							$("#size-label").empty();
							$("#size-label").append("Fourth-Axis");
						
							$('#x-axis').empty().append('<option value="0">None</option></select>');
							$('#y-axis').empty().append('<option value="0">None</option></select>');
							$('#color-axis').empty().append('<option value="0">None</option></select>');
							$('#size-axis').empty().append('<option value="0">None</option></select>');
						}
						
						$('#x-axis').append('<option value="' + ++index + '">' + value + '</option>');
						$('#y-axis').append('<option value="' + ++index + '">' + value + '</option>');
						$('#color-axis').append('<option value="' + ++index + '">' + value + '</option>');
						$('#size-axis').append('<option value="' + ++index + '">' + value + '</option>');

						counter ++;
					});
			  }
	); 
}
function scatterDisplay() {	
	$("#x-axis").parent('li').show();
	$("#x-label").show();
	$("#x-label").empty();
	$("#x-label").append("X-Axis");
	$("#y-axis").parent('li').show();
	$("#y-label").show();
	$("#y-label").empty();
	$("#y-label").append("Y-Axis");
	$("#color-axis").parent('li').show();
	$("#color-label").show();
	$("#color-label").empty();
	$("#color-label").append("Color");
	$("#size-axis").parent('li').show();
	$("#size-label").show();
	$("#size-label").empty();
	$("#size-label").append("Size");
	
	$('#x-axis').empty().append('<option value="0">None</option></select>');
	$('#y-axis').empty().append('<option value="0">None</option></select>');
	$('#color-axis').empty().append('<option value="0">None</option></select>');
	$('#size-axis').empty().append('<option value="0">None</option></select>');
	
	var x_index = 0;
	var y_index = 0;
	var color_index = 0;
	var size_index = 0;
	
	$.each(selectColumns_Name_Value, function( index, value ) {
		
		$('#x-axis').append('<option value="' + ++x_index + '">' + index + '</option>');
		
		$('#y-axis').append('<option value="' + ++y_index + '">' + index + '</option>');	
		
		if(value == "Nominal") {
			$('#color-axis').append('<option value="' + ++color_index + '">' + index + '</option>');	
		}
		
		if(value == "Ratio" || value == "Interval") {
			$('#size-axis').append('<option value="' + ++size_index + '">' + index + '</option>');	
		}
		
	});
		
}
function scatterDisplay2(){
	
	var result=[];
	$.get("getDerivedFields", function(data){
				   	result = data;
				   	var counter = 0;
					$.each( result, function( index, value ) {
						if (counter == 0){
							$("#x-axis").parent('li').show();
							$("#x-label").show();
							$("#x-label").empty();
							$("#x-label").append("X-Axis");
							$("#y-axis").parent('li').show();
							$("#y-label").show();
							$("#y-label").empty();
							$("#y-label").append("Y-Axis");
							$("#color-axis").parent('li').hide();
							$("#color-label").hide();
							$("#color-label").empty();
							$("#color-label").append("Color");
							$("#size-axis").parent('li').hide();
							$("#size-label").hide();
							$("#size-label").empty();
							$("#size-label").append("Size");
						
							$('#x-axis').empty().append('<option value="0">None</option></select>');
							$('#y-axis').empty().append('<option value="0">None</option></select>');
							$('#color-axis').empty().append('<option value="0">None</option></select>');
							$('#size-axis').empty().append('<option value="0">None</option></select>');
						}
						
						$('#x-axis').append('<option value="' + ++index + '">' + value + '</option>');
						$('#y-axis').append('<option value="' + ++index + '">' + value + '</option>');
						counter ++;
					});
			  }
	); 
}

function pieDisplay() {
	$("#x-axis").parent('li').hide();
	$("#x-label").hide();
	$("#y-axis").parent('li').hide();
	$("#y-label").hide();
	$("#color-axis").parent('li').show();
	$("#color-label").show();
	$("#color-label").empty();
	$("#color-label").append("Color");
	$("#size-axis").parent('li').show();
	$("#size-label").show();
	$("#size-label").empty();
	$("#size-label").append("Size");
	
	var x_index = 0;
	var y_index = 0;
	var color_index = 0;
	var size_index = 0;
	
	$('#x-axis').empty().append('<option value="0">None</option></select>');
	$('#y-axis').empty().append('<option value="0">None</option></select>');
	$('#color-axis').empty().append('<option value="0">None</option></select>');
	$('#size-axis').empty().append('<option value="0">None</option></select>');
	
	$.each(selectColumns_Name_Value, function( index, value ) {
		
		
		$('#x-axis').append('<option value="' + ++x_index + '">' + index + '</option>');
		
		$('#y-axis').append('<option value="' + ++y_index + '">' + index + '</option>');	
		
		if(value == "Nominal") {
			$('#color-axis').append('<option value="' + ++color_index + '">' + index + '</option>');	
		}
		
		if(value == "Ratio" || value == "Interval") {
			$('#size-axis').append('<option value="' + ++size_index + '">' + index + '</option>');	
		}
		
	});
	
}

function pieDisplay2(){
	
	var result=[];
	$.get("getDerivedFields", function(data){
				   	result = data;
				   	var counter = 0;
					$.each( result, function( index, value ) {
						if (counter == 0){
							$("#x-axis").parent('li').hide();
							$("#x-label").hide();
							$("#y-axis").parent('li').hide();
							$("#y-label").hide();
							$("#color-axis").parent('li').show();
							$("#color-label").show();
							$("#color-label").empty();
							$("#color-label").append("Color");
							$("#size-axis").parent('li').show();
							$("#size-label").show();
							$("#size-label").empty();
							$("#size-label").append("Size");

							$('#x-axis').empty().append('<option value="0">None</option></select>');
							$('#y-axis').empty().append('<option value="0">None</option></select>');
							$('#color-axis').empty().append('<option value="0">None</option></select>');
							$('#size-axis').empty().append('<option value="0">None</option></select>');
						}
						
						if((value.indexOf('SUM') > -1)||( value.indexOf('COUNT') > -1)||
								(value.indexOf('AVG') > -1)||(value.indexOf('MIN') > -1)||
								(value.indexOf('MAX') > -1)){
							$('#size-axis').append('<option value="' + ++index + '">' + value + '</option>');
						}
						else
							$('#color-axis').append('<option value="' + ++index + '">' + value + '</option>');
						
						counter ++;
					});
			  }
	); 
}

function barDisplay() {
	$("#x-axis").parent('li').show();
	$("#x-label").show();
	$("#x-label").empty();
	$("#x-label").append("X-Axis");
	$("#y-axis").parent('li').show();
	$("#y-label").show();
	$("#y-label").empty();
	$("#y-label").append("Y-Axis");
	$("#color-axis").parent('li').hide();
	$("#color-label").hide();
	$("#size-axis").parent('li').hide();
	$("#size-label").hide();
	
	var x_index = 0;
	var y_index = 0;
	var color_index = 0;
	var size_index = 0;
	
	$('#x-axis').empty().append('<option value="0">None</option></select>');
	$('#y-axis').empty().append('<option value="0">None</option></select>');
	$('#color-axis').empty().append('<option value="0">None</option></select>');
	$('#size-axis').empty().append('<option value="0">None</option></select>');
	
	console.log("selectColumns_Name_Value" + selectColumns_Name_Value);
	
	$.each(selectColumns_Name_Value, function( index, value ) {
		
		$('#x-axis').append('<option value="' + ++x_index + '">' + index + '</option>');
		
		$('#y-axis').append('<option value="' + ++y_index + '">' + index + '</option>');	
		
		if(value == "Nominal") {
			$('#color-axis').append('<option value="' + ++color_index + '">' + index + '</option>');	
		}
		
		if(value == "Ratio" || value == "Interval") {
			$('#size-axis').append('<option value="' + ++size_index + '">' + index + '</option>');	
		}
			
	});
	
}

function barDisplay2(){
	
	var result=[];
	$.get("getDerivedFields", function(data){
				   	result = data;
				   	var counter = 0;
					$.each( result, function( index, value ) {
						if (counter == 0){
							$("#x-axis").parent('li').show();
							$("#x-label").show();
							$("#x-label").empty();
							$("#x-label").append("X-Axis");
							$("#y-axis").parent('li').show();
							$("#y-label").show();
							$("#y-label").empty();
							$("#y-label").append("Y-Axis");
							$("#color-axis").parent('li').hide();
							$("#color-label").hide();
							$("#size-axis").parent('li').hide();
							$("#size-label").hide();
							$('#x-axis').empty().append('<option value="0">None</option></select>');
							$('#y-axis').empty().append('<option value="0">None</option></select>');
							$('#color-axis').empty().append('<option value="0">None</option></select>');
							$('#size-axis').empty().append('<option value="0">None</option></select>');
						}
						
						if((value.indexOf('SUM') > -1)||( value.indexOf('COUNT') > -1)||
								(value.indexOf('AVG') > -1)||(value.indexOf('MIN') > -1)||
								(value.indexOf('MAX') > -1)){
							$('#y-axis').append('<option value="' + ++index + '">' + value + '</option>');
						}
						else
							$('#x-axis').append('<option value="' + ++index + '">' + value + '</option>');
							counter ++;
					});
			  }
	); 
}

function lineDisplay() {
	$("#x-axis").parent('li').show();
	$("#x-label").show();
	$("#x-label").empty();
	$("#x-label").append("X-Axis");
	$("#y-axis").parent('li').show();
	$("#y-label").show();
	$("#y-label").empty();
	$("#y-label").append("Y-Axis");
	$("#color-axis").parent('li').hide();
	$("#color-label").hide();
	$("#size-axis").parent('li').hide();
	$("#size-label").hide();
	
	var x_index = 0;
	var y_index = 0;
	var color_index = 0;
	var size_index = 0;
	
	$('#x-axis').empty().append('<option value="0">None</option></select>');
	$('#y-axis').empty().append('<option value="0">None</option></select>');
	$('#color-axis').empty().append('<option value="0">None</option></select>');
	$('#size-axis').empty().append('<option value="0">None</option></select>');
	
	$.each(selectColumns_Name_Value, function( index, value ) {
		
		$('#x-axis').append('<option value="' + ++x_index + '">' + index + '</option>');
		
		$('#y-axis').append('<option value="' + ++y_index + '">' + index + '</option>');	
		
		if(value == "Nominal") {
			$('#color-axis').append('<option value="' + ++color_index + '">' + index + '</option>');	
		}
		
		if(value == "Ratio" || value == "Interval") {
			$('#size-axis').append('<option value="' + ++size_index + '">' + index + '</option>');	
		}
			
	});
	
}

function lineDisplay2() {
	var result=[];
	$.get("getDerivedFields", function(data){
				   	result = data;
				   	var counter = 0;
					$.each( result, function( index, value ) {
						if (counter == 0){
							$("#x-axis").parent('li').show();
							$("#x-label").show();
							$("#x-label").empty();
							$("#x-label").append("X-Axis");
							$("#y-axis").parent('li').show();
							$("#y-label").show();
							$("#y-label").empty();
							$("#y-label").append("Y-Axis");
							$("#color-axis").parent('li').hide();
							$("#color-label").hide();
							$("#size-axis").parent('li').hide();
							$("#size-label").hide();
							$('#x-axis').empty().append('<option value="0">None</option></select>');
							$('#y-axis').empty().append('<option value="0">None</option></select>');
							$('#color-axis').empty().append('<option value="0">None</option></select>');
							$('#size-axis').empty().append('<option value="0">None</option></select>');
						}
						
						$('#x-axis').append('<option value="' + ++index + '">' + value + '</option>');					
						$('#y-axis').append('<option value="' + ++index + '">' + value + '</option>');
						counter ++;
					});
			  }
	); 
}