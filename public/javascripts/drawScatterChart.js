
/**
 * Draw scatter chart
 */

function scatterPlot2(data2, selected_dimensions, columns_string, data_scale) {


	
	var data3 = [];
	for (i = 0; i < data2[0].length; i++) {
		data3[i] = {
			first : data2[0][i],
			second : data2[1][i]
		};
	}
	console.log(data2[0].length);
	d3.select("svg").remove();
	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();
	var margin = {
		top : 20,
		right : 20,
		bottom : 30,
		left : 40
	},
	width = 900 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

	var x = d3.scale.linear()
		.range([0, width - 100]);

	var y = d3.scale.linear()
		.range([height, 0]);

	var color = d3.scale.category10();

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");

	var svg = d3.select("#chart").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	data3.forEach(function (d) {
		d.second = +d.second;
		d.first = +d.first;
	});

	x.domain(d3.extent(data3, function (d) {
			return d.first;
		})).nice();
	y.domain(d3.extent(data3, function (d) {
			return d.second;
		})).nice();

	svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + height + ")")
	.call(xAxis)
	.append("text")
	.attr("class", "label")
	.attr("x", width - 100)
	.attr("y", -6)
	.style("text-anchor", "end")
	.text(columns_string[0]);

	svg.append("g")
	.attr("class", "y axis")
	.call(yAxis)
	.append("text")
	.attr("class", "label")
	.attr("transform", "rotate(-90)")
	.attr("y", 6)
	.attr("dy", ".71em")
	.style("text-anchor", "end")
	.text(columns_string[1])

	var div = d3.select("body").append("div")
		.attr("class", "tooltip")
		.style("opacity", 0);
	
	
	// draw tip
	var tip = d3.tip()
	.attr('class', 'd3-tip')
	.offset([-10, 0])
	.html(function (d) {
		return "<strong>" + columns_string[0] + ":</strong> <span style='color:red'>" + "<strong>" + d.first + "</strong>" + "</span>" + " " + "<strong>" + columns_string[1] + ":</strong> <span style='color:red'>" + "<strong>" + d.second + "</strong> </span>";
		
	})
	
	svg.call(tip);
	
	svg.selectAll(".dot")
	.data(data3)
	.enter().append("circle")
	.attr("class", "dot")
	.attr("r", 3.5)
	.attr("cx", function (d) {
		return x(d.first);
	})
	.attr("cy", function (d) {
		return y(d.second);
	})
	.style("fill", "steelblue")
	.on('mouseover', tip.show)
	.on('mouseout', tip.hide)



}

//  scatterplot3
//
//
//
//
function scatterPlot3(data2, selected_dimensions, columns_string) {
	var data3 = [];
	for (i = 0; i < data2[0].length; i++) {
		data3[i] = {
			first : data2[0][i],
			second : data2[1][i],
			third : data2[2][i]
		};
	}

	d3.select("svg").remove();
	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();
	var margin = {
		top : 20,
		right : 20,
		bottom : 30,
		left : 40
	},
	width = 900 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

	var x = d3.scale.linear()
		.range([0, width - 100]);

	var y = d3.scale.linear()
		.range([height, 0]);

	var r = d3.scale.linear()
		.range([0, 4.0]);

	var color = d3.scale.category10();

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");

	var svg = d3.select("#chart").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	var div = d3.select("body").append("div")
		.attr("class", "tooltip")
		.style("opacity", 0);

	// draw tip
	var tip = d3.tip()
	.attr('class', 'd3-tip')
	.offset([-10, 0])
	.html(function (d) {
		return "<strong>" + columns_string[2] + ":</strong> <span style='color:red'>" + "<strong>" + d.third + "</strong> </span> <br>" + "<strong>" + columns_string[0] + ":</strong> <span style='color:red'>" + "<strong>" + d.first + "</strong>" + "</span>" + " " + "<strong>" + columns_string[1] + ":</strong> <span style='color:red'>" + "<strong>" + d.second + "</strong> </span> </br>";

	})
	
	svg.call(tip);
	
	
	data3.forEach(function (d) {
		d.first = +d.first;
		d.second = +d.second;
	});

	x.domain(d3.extent(data3, function (d) {
			return d.first;
		})).nice();
	y.domain(d3.extent(data3, function (d) {
			return d.second;
		})).nice();
	r.domain(d3.extent(data3, function (d) {
			return d.third;
		})).nice();

	svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + height + ")")
	.call(xAxis)
	.append("text")
	.attr("class", "label")
	.attr("x", width - 100)
	.attr("y", -6)
	.style("text-anchor", "end")
	.text(columns_string[0]);

	svg.append("g")
	.attr("class", "y axis")
	.call(yAxis)
	.append("text")
	.attr("class", "label")
	.attr("transform", "rotate(-90)")
	.attr("y", 6)
	.attr("dy", ".71em")
	.style("text-anchor", "end")
	.text(columns_string[1])

	if (selected_dimensions[2] === "color-axis") {
		svg.selectAll(".dot")
		.data(data3)
		.enter().append("circle")
		.attr("class", "dot")
		.attr("r", 3.5)
		.attr("cx", function (d) {
			return x(d.first);
		})
		.attr("cy", function (d) {
			return y(d.second);
		})
		.style("fill", function (d) {
			return color(d.third);
		})
	.on('mouseover', tip.show)
	.on('mouseout', tip.hide)
				

	} else {
		svg.selectAll(".dot")
		.data(data3)
		.enter().append("circle")
		.attr("class", "dot")
		.attr("r", function (d) {
			return r(d.third*2);
		})
		.attr("cx", function (d) {
			return x(d.first);
		})
		.attr("cy", function (d) {
			return y(d.second);
		})
		.style("fill", "steelblue")

	.on('mouseover', tip.show)
	.on('mouseout', tip.hide)

	}

	var legend = svg.selectAll(".legend")
		.data(color.domain())
		.enter().append("g")
		.attr("class", "legend")
		.attr("transform", function (d, i) {
			return "translate(0," + i * 20 + ")";
		});

	legend.append("rect")
	.attr("x", width - 18)
	.attr("width", 18)
	.attr("height", 18)
	.style("fill", color);

	legend.append("text")
	.attr("x", width - 24)
	.attr("y", 9)
	.attr("dy", ".35em")
	.style("text-anchor", "end")
	.text(function (d) {
		return d;
	});

}

//  scatterplot4
//
//
//
//
function scatterPlot4(data2, selected_dimensions, columns_string) {
	var data3 = [];
	
	for (i = 0; i < data2[0].length; i++) {
		data3[i] = {
			first : data2[0][i],
			second : data2[1][i],
			third : data2[2][i],
			fourth : data2[3][i]
		};
	}

	d3.select("svg").remove();
	var myElem = document.getElementById('pdiv');
	if (myElem != null)
		myElem.remove();
	var margin = {
		top : 20,
		right : 20,
		bottom : 30,
		left : 40
	},
	width = 900 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

	var x = d3.scale.linear()
		.range([0, width - 100]);

	var y = d3.scale.linear()
		.range([height, 0]);

	var r = d3.scale.linear()
		.range([0, 5.0]);

	var color = d3.scale.category10();

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");

	var svg = d3.select("#chart").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	data3.forEach(function (d) {
		d.first = +d.first;
		d.second = +d.second;
	});

	x.domain(d3.extent(data3, function (d) {
			return d.first;
		})).nice();
	y.domain(d3.extent(data3, function (d) {
			return d.second;
		})).nice();
	r.domain(d3.extent(data3, function (d) {
			return d.fourth;
		})).nice();

	svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + height + ")")
	.call(xAxis)
	.append("text")
	.attr("class", "label")
	.attr("x", width - 100)
	.attr("y", -6)
	.style("text-anchor", "end")
	.text(columns_string[0]);

	svg.append("g")
	.attr("class", "y axis")
	.call(yAxis)
	.append("text")
	.attr("class", "label")
	.attr("transform", "rotate(-90)")
	.attr("y", 6)
	.attr("dy", ".71em")
	.style("text-anchor", "end")
	.text(columns_string[1])

	var div = d3.select("body").append("div")
		.attr("class", "tooltip")
		.style("opacity", 0);

	var tip = d3.tip()
	.attr('class', 'd3-tip')
	.offset([-10, 0])
	.html(function (d) {
		return	"<strong>" + columns_string[2] + ":</strong> <span style='color:red'>" + "<strong>" + d.third + "</strong> </span> <br>" + "<strong>" + columns_string[0] + ":</strong> <span style='color:red'>" + "<strong>" + d.first + "</strong>" + "</span>" + " " + "<strong>" + columns_string[1] + ":</strong> <span style='color:red'>" + "<strong>" + d.second + "</strong> </span> </br>";

	})
	
	svg.call(tip);
	
	svg.selectAll(".dot")
	.data(data3)
	.enter().append("circle")
	.attr("class", "dot")
	.attr("r", function (d) {
		return r(d.fourth);
	})
	.attr("cx", function (d) {
		return x(d.first);
	})
	.attr("cy", function (d) {
		return y(d.second);
	})
	.style("fill", function (d) {
		return color(d.third);
	})
	.on('mouseover', tip.show)
	.on('mouseout', tip.hide)
		

	var legend = svg.selectAll(".legend")
		.data(color.domain())
		.enter().append("g")
		.attr("class", "legend")
		.attr("transform", function (d, i) {
			return "translate(0," + i * 20 + ")";
		});

	legend.append("rect")
	.attr("x", width - 18)
	.attr("width", 18)
	.attr("height", 18)
	.style("fill", color);

	legend.append("text")
	.attr("x", width - 24)
	.attr("y", 9)
	.attr("dy", ".35em")
	.style("text-anchor", "end")
	.text(function (d) {
		return d;
	});

}
