/* edit on 2014117 */

package controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import play.db.DB;
import play.mvc.Controller;

import com.google.gson.Gson;
import mdsj.MDSJ;


public class Application extends Controller {
	
	/**
	 * "DROP TABLE IF EXISTS CSV_DATA"
	 */
	public static String dropTable = "DROP TABLE IF EXISTS CSV_DATA";
	
	/**
	 * "CREATE TABLE CSV_DATA "
	 */
	public static String createTable = "CREATE TABLE CSV_DATA ";
	
	/**
	 * "(ID int NOT NULL AUTO_INCREMENT,"
	 */
	public static String autoIncrementID = "(ID int NOT NULL AUTO_INCREMENT,";
	
	/**
	 * "PRIMARY KEY (ID));"
	 */
	public static String primaryKey = "PRIMARY KEY (ID));";
	
	/**
	 * Column Type : varchar(255)
	 */
	public static String columnType = " varchar(255)";
	
	/**
	 * 	"INSERT INTO CSV_DATA ("
	 */
	public static String insertStmt = "INSERT INTO CSV_DATA (";
	
	/**
	 * 
	 */
	public static final String select = "SELECT ";
		
	/**
	 * List of Column names
	 */
	public static List<String> columnNames;
	
	/**
	 * Columns with comma separated values
	 */
	public static String columns;
	/**
	 * map for storing calculation queries
	 */	 
	public static HashMap<String, String> calcQueries = new HashMap<String, String>(); 
	
	/*
	 
	*/
	

	/***
	 * Method for rendering Home [First] page
	 */
	public static void home(){
		render();
	}
	
	/***
	 * Method for rendering Selection [Second] page
	 */
    public static void selection() {
    	
    	if(null != columnNames) {
    		String columns = "";
    		for(int i=0; i< columnNames.size();i++) {
        		columns = columns + columnNames.get(i) + ",";
        	}
    		columns = columns.substring(0, columns.length()-1);
    		render(columns);   		
    	}
    }
    
    /***
	 * Method for parsing CSV file
	 */    
	public static void parseCSV(File CSVFile) throws IOException, SQLException{
		
		if (CSVFile==null){
			validation.required(CSVFile);
			if(validation.hasErrors())
				render("@home");
		}
	
		BufferedReader bufferReader = null;
		String line = "";
		String csvDelimiter = ",";
		
		Connection conn = DB.getConnection();
		
		Statement stmt = conn.createStatement();
	 
		try {
	 
			int counter = 0; 
			bufferReader = new BufferedReader(new FileReader(CSVFile));
			while ((line = bufferReader.readLine()) != null) {
	 		
				if((counter++) == 0) {					
					columnNames = Arrays.asList(line.split(csvDelimiter));
					
					stmt.executeUpdate(dropTable);
						
					//Create query string for table creation
					String queryString = createTable + autoIncrementID;
					
					for (String column : columnNames) {
						queryString = queryString + column + columnType + ",";				
					}
					queryString = queryString + primaryKey;		
					
					//Create Table
					stmt.executeUpdate(queryString);
					continue;
				}
		        
		        String[] columnValue = line.split(csvDelimiter);
								
				String insertString = insertStmt;
				
				for (String column : columnNames) {
					insertString = insertString + column + ",";				
				}
				
				insertString = insertString.substring(0, insertString.length()-1);
				
				insertString = insertString + ") VALUES (";	
				
				for (String column : columnValue) {
					column = column.replace("'", "");
					insertString = insertString + "'" + column+ "',";
				}
				
				insertString = insertString.substring(0, insertString.length()-1) + ")";
				//Create Table
				stmt.executeUpdate(insertString);
			}
	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bufferReader != null) {
				try {
					bufferReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	
		selection();
	}
	
	/**
	 * Builds data map using data from database
	 */
	public static void buildDataMap(){
		// For Sending JSON Data
		ArrayList<ArrayList<Object>> table = new ArrayList<ArrayList<Object>>();
		List entryname = new ArrayList<String>();
		ArrayList<ArrayList<Object>> table2 = new ArrayList<ArrayList<Object>>();
		
		
		java.sql.Connection conn = DB.getConnection();

		// Create query string
		String query = "select *";

		String table_name = "CSV_DATA";
		query = query + " from " + table_name;

		try {
			ResultSet rs = conn.createStatement().executeQuery(query);
			// number of columns

			for (int i = 0; i < columnNames.size(); i++) {
				table.add(new ArrayList<Object>());
			}
			while (rs.next()) {
				int i = 0;
				for (String s : columnNames) {
					table.get(i++).add(rs.getObject(s));
				}

			}

		} catch (SQLException e) {
			System.out.println("@select columns, columns: Cannot be selected");
			e.printStackTrace();
		}
		
		HashMap<String,ArrayList<Double>> numericList=new HashMap<String, ArrayList<Double>>();
		HashMap<String,ArrayList<String>> norminalList=new HashMap<String, ArrayList<String>>();
		for(int i=0;i<table.get(1).size();i++){
			numericList.put((String) table.get(1).get(i), new ArrayList<Double>());
			norminalList.put((String) table.get(1).get(i), new ArrayList<String>());
			
			
			entryname.add((String) table.get(1).get(i));
				
		}
		for(int i=0;i<table.size();i++){
			if(i==1){
				continue;
			}
			try{
				Double numeric=Double.parseDouble((String) table.get(i).get(0));
				for(int j=0;j<table.get(i).size();j++){
					Double valueDouble=Double.parseDouble((String) table.get(i).get(j));
					numericList.get(table.get(1).get(j)).add(valueDouble);
				}
				
			}catch(Exception e){
				for(int j=0;j<table.get(i).size();j++){
					norminalList.get(table.get(1).get(j)).add((String) table.get(i).get(j));
				}
			}
		}
		/**
		 */
		Gson gson=new Gson();
		
		double[][] input=new double[numericList.size()][numericList.size()];
		
		List<String[]> listOfValue = new ArrayList<String[]>();
		
		
		
		int i=0;
		int j=0;
		List<String> finalColumns = new ArrayList<String>();

		for(Entry<String, ArrayList<Double>> entryRow:numericList.entrySet()){
			finalColumns.add(entryRow.getKey());
			String[] rank = new String[3];
			rank[0] = numericList.get(entryRow.getKey()).get(3).toString();
			rank[1] = numericList.get(entryRow.getKey()).get(0).toString();
			rank[2] = numericList.get(entryRow.getKey()).get(1).toString();
			listOfValue.add(rank);
			for(Entry<String, ArrayList<Double>> entryColumn:numericList.entrySet()){
				Double powdis = (double) 0;
				Double Nomnumdis = (double) 0;
				Double Nomdis = (double) 0;
				Double dis = (double) 0;
				for(int m=0;m<numericList.get(entryRow.getKey()).size();m++){
					Double max=Double.MIN_VALUE;
					Double min=Double.MAX_VALUE;			
					for(Entry<String, ArrayList<Double>> find:numericList.entrySet()){
						if(find.getValue().get(m)>max){
							max=find.getValue().get(m);
						}
						if(find.getValue().get(m)<min){
							min=find.getValue().get(m);
						}
					}
					 double numerdis = numericList.get(entryRow.getKey()).get(m)-numericList.get(entryColumn.getKey()).get(m);
					 double scale= max-min;
					 
					 powdis = Math.pow(numerdis/scale,2);
					 
					 Nomnumdis += powdis;
				}
				
				for(int m=0;m<norminalList.get(entryRow.getKey()).size();m++){
					double nomidis = (double) 0;
					if ((norminalList.get(entryRow.getKey()).get(m)).equals(norminalList.get(entryColumn.getKey()).get(m)))
					{			
						nomidis = (double) 0;
					}
					else
					{
						nomidis = (double) 1;
					}
					Nomdis += nomidis;
				}
				
				dis = (Nomdis+Nomnumdis);
				dis = Math.sqrt(dis);
				input[i][j]= dis;
				
				j++;
			}
			i++;
			j=0;
		}
		
		double[][] output=MDSJ.classicalScaling(input);
          
		int n=input[0].length;    // number of data objects

		
	    //entryname
	    for(int g=0; g<n; g++){
	    
	    	table2.add(new ArrayList<Object>());
	    
	    }
	       int g=0;
	       int count = n;
	    	while(count>0){
	    	
	    		table2.get(g).add(finalColumns.get(g));
	    		table2.get(g).add(output[0][g]);
	    		table2.get(g).add(output[1][g]);
	    		table2.get(g).add(listOfValue.get(g)[0]);
	    		table2.get(g).add(listOfValue.get(g)[1]);
	    		table2.get(g).add(listOfValue.get(g)[2]);
	    		g++;
	    		count--;
	    	}
	    
		
		renderJSON(table2);
		
		
	}
	
	
	/***
	 * Method for initializing the UI table with data 
	 */
	public static void initialize_table(ArrayList<String> columns) {

		ArrayList<ArrayList<Object>> table = new ArrayList<ArrayList<Object>>();

		java.sql.Connection conn = DB.getConnection();

		// making query string
		String query = "SELECT ";
		for (String s : columns)
			query = query + s + ",";

		query = query.substring(0, query.length() - 1);

		query = query + " FROM CSV_DATA";
		try {
			ResultSet rs = conn.createStatement().executeQuery(query);

			for (int i = 0; i < columns.size(); i++) {
				table.add(new ArrayList<Object>());
			}
			while (rs.next()) {
				int i = 0;
				for (String s : columns) {
					table.get(i++).add(rs.getObject(s));
				}
			}

		} catch (SQLException e) {
			System.out.println("@select columns, columns: Cannot be selected");
			e.printStackTrace();
		}
		
		renderJSON(table);
	}
	
	/***
	 * Method for retrieving the data for selected columns 
	 */
	public static void selectColumns(ArrayList<String> columns){
	// For Sending JSON Data
	ArrayList<ArrayList<Object>> table = new ArrayList<ArrayList<Object>>();
	
	java.sql.Connection conn = DB.getConnection();
	
	//Create query string
	String query = "select ";
	for (String s : columns)
		query = query + s + ",";
	query = query.substring(0, query.length()-1);
	String table_name = "CSV_DATA";
	query = query + " from " + table_name;
	
	String keyBuilder= "";
	String tmp1="";
	String tmp2="";
	for (String s : columns){
		if(s.contains("AVG")||s.contains("SUM")||s.contains("MIN")||s.contains("MAX")||s.contains("COUNT"))
			tmp1 = s;
		else 
			tmp2 = s;
	}
	keyBuilder = tmp1 +"-"+tmp2;
	
	if(calcQueries.containsKey(keyBuilder))
		query = calcQueries.get(keyBuilder);
	
	try {
			ResultSet rs = conn.createStatement().executeQuery(query);
			
			for (int i=0; i<columns.size();i++)
			{
				table.add(new ArrayList<Object>());
			}
			while (rs.next())
			{
				int i = 0;
				for (String s: columns)
				{
					table.get(i++).add(rs.getObject(s));
				}
				
			}
			
			} catch (SQLException e) {
			System.out.println("@select columns, columns: Cannot be selected");
			e.printStackTrace();
			}
	
	    renderJSON(table);
	}


	/***
	 * Method for retrieving all the data 
	 */

	public static void selectAll(){
	// For Sending JSON Data
	ArrayList<ArrayList<Object>> table = new ArrayList<ArrayList<Object>>();
	
	java.sql.Connection conn = DB.getConnection();
	
	//Create query string
	String query = "select *";

	String table_name = "CSV_DATA";
	query = query + " from " + table_name;
	
	try {
			ResultSet rs = conn.createStatement().executeQuery(query);
			//number of columns		
			
			for (int i=0; i<columnNames.size();i++)
			{
				table.add(new ArrayList<Object>());
			}
			while (rs.next())
			{
				int i = 0;
				for (String s: columnNames)
				{
					table.get(i++).add(rs.getObject(s));
				}
				
			}
			
			} catch (SQLException e) {
			System.out.println("@select columns, columns: Cannot be selected");
			e.printStackTrace();
			}
	
	    renderJSON(table);
	}

	    
	/***
	 * Method for retrieving query of the calculation choice 
	 */
	
	public static void buildQuery(ArrayList<String> querySelectParts){
		
		//set the group value
		String group = querySelectParts.get(0);
		
		// clear the previous map
		calcQueries.clear();
		int counter= 0;
	
		for (String s : querySelectParts){
			counter++;
			if(counter==1)
				continue;
			//Create query string
			String query = "SELECT ";
			query = query + group + ","+s;
			String table_name = "CSV_DATA ";
			query = query + " FROM " + table_name;
		
			if(querySelectParts.size() >=2)
				query = query + "GROUP BY "+ querySelectParts.get(0);
		
			//print Query
			//Building Key for HashMap of queries and a derived field to show in visualization page 
			String key= "";
			key = s +"-" + group;
		
		// add query to hashmap  
		calcQueries.put(key, query);
		}
		
		
		
	}
	
	public static void getDerivedFields(){
		
		ArrayList<Object> derivedFields= new ArrayList<Object>();

		for ( String key : calcQueries.keySet() ) {
			
			String first = "";
			String second ="";
			if(key.contains("-"))
			{
				first = key.substring(0, key.indexOf("-"));
				second = key.substring(key.indexOf("-")+1);
			}
				
			if(!derivedFields.contains(first))					
				derivedFields.add(first);
			
			if(!derivedFields.contains(second))	
				derivedFields.add(second);
		}
		
		renderJSON(derivedFields);
	}

}
